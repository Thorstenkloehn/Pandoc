package main

import (
	"html"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"

	"os/exec"
	"strings"
)

var (
	vorlagen, _ = template.ParseGlob("vorlagen/*")
)

func Pandoc(eingabe, ausgabe, Text string) string {

	start := exec.Command("pandoc", "-f", eingabe, "-t", ausgabe)
	start.Stdin = strings.NewReader(Text)
	test, _ := start.CombinedOutput()
	start.Run()
	return html.EscapeString(string(test))
}

func main() {
	router := mux.NewRouter()
	s := http.StripPrefix("/static/", http.FileServer(http.Dir("static")))
	router.PathPrefix("/static/").Handler(s)
	router.HandleFunc("/", Start).Methods("GET")
	router.HandleFunc("/", Ausgabetext).Methods("POST")
	http.ListenAndServe(":8001", router)
}

func Start(w http.ResponseWriter, r *http.Request) {
	vorlagen.ExecuteTemplate(w, "index.htm", nil)

}
func Ausgabetext(w http.ResponseWriter, r *http.Request) {

	eingabe := r.FormValue("eingabe")
	ausgabe := r.FormValue("ausgabe")
	text := r.FormValue("test")

	ergebniss := Pandoc(eingabe, ausgabe, text)
	vorlagen.ExecuteTemplate(w, "startseite.htm", ergebniss)

}
